@extends('layout.master')

@section('categories')

<form action="/jawaban/{{$jawaban->id}}" method="post">
  @csrf
  @method('put')
  <div class="form-group">
      <textarea name="content"  class="form-control @error('content') is-invalid         
      @enderror" cols="30" rows="10">{{$jawaban->content}}</textarea>
  </div>
  <button type="submit" class="btn btn-primary">Edit Komentar</button>
  
</form>
@endsection