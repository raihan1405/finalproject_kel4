@extends('layout.master')

@section('categories')
        <table class="table my-3">
            <tbody>
              <tr>
                <th scope="row">Nama</th>
                <td>{{$user->name}}</td>
              </tr>
              <tr>
                <th scope="row">umur</th>
                <td>{{$profile->umur}}</td>
              </tr>
              <tr>
                <th scope="row">biodata</th>
                <td>{{$profile->biodata}}</td>
              </tr>
              <tr>
                <th scope="row">email</th>
                <td>{{$profile->email}}</td>
              </tr>
              <tr>
                <th scope="row">alamat</th>
                <td>{{$profile->alamat}}</td>
              </tr>
            </tbody>
          </table>
          <a href="/profile/{{$profile->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
  
@endsection