@extends('layout.master')

@section('categories')
<form action="/profile/{{$profile->id}}" method="post">
    @csrf
    @method('put')
    <div>
        <label> Nama </label>
        <p>{{$user->name}}</p>
        
    </div>
    <div class="form-group my-3">
      <label>Umur</label>
      <input type="number" name="umur" value="{{$profile->umur}}" class="form-control @error('umur') is-invalid   
      @enderror" >
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label >Biodata</label>
      <textarea type="text" name="biodata"  class="form-control @error('biodata') is-invalid   
      @enderror">{{$profile->biodata}}</textarea>
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label >Email</label>
      <input type="text" name="email" value="{{$profile->email}}" class="form-control @error('email') is-invalid   
      @enderror">
    </div>
    @error('email')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label >alamat</label>
      <input type="text" name="alamat" value="{{$profile->alamat}}" class="form-control @error('alamat') is-invalid   
      @enderror">
    </div>
    @error('alamat')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">update profile</button>
  </form> 
@endsection