@extends('layout.master')

@section('categories')

<img src="{{asset('uploads/'. $question->gambar)}}" width="100%" height="500px" alt="">
<p>{{$question->tulisan}}</p>


<hr>

<h4>List Komentar</h4>
@forelse ($question->jawaban as $item)
<div class="card my-3">
    <div class="card-body">
      <h5 class="card-title">{{$item->User->name}}</h5>
      <p class="card-text">{{$item->content}}</p>
        <div class="text-right">
            @auth
            <form action="/jawaban/{{$item->id}}"
            method="POST">
            @csrf
            @method('delete')
            <input type="submit" class="btn btn-danger btn-sm" value="Delete">
            <a href="/jawaban/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
            @endauth
        </div>
    </div>
</div>
@empty
    <h4>Tidak Ada Komentar</h4>
@endforelse
<hr>

@auth
    

    
{{-- Form Jawaban --}}
<form action="/jawaban/{{$question->id}}" method="post">
    @csrf
    <div class="form-group">
        
        <textarea name="content" placeholder="isi jawaban" class="form-control @error('content') is-invalid         
        @enderror" cols="30" rows="10"></textarea>
    </div>
    <button type="submit" class="btn btn-primary">Tambah Komentar</button>
    
</form>
@endauth
@endsection