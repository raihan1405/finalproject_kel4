@extends('layout.master')

@section('categories')
<form action="/question" method="post" enctype="multipart/form-data">
    @csrf
    <div class="form-group my-3">
      <label>question</label>
      <textarea type="text" name="tulisan" class="form-control @error('tulisan') is-invalid   
      @enderror" ></textarea>
    </div>
    @error('tulisan')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>gambar</label>
      <input type="file" name="gambar" class="form-control @error('gambar') is-invalid   
      @enderror" >
    </div>
    @error('gambar')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>kategori</label>
      <select name="category_id" class="form-control" id="">
      <option value="">--pilih kategori--</option>
      @forelse ($categorie as $item)
          <option value="{{$item->id}}">{{$item->name}}</option>
      @empty
          <option value="">Tidak ada kategori</option>
      @endforelse
    </select>
    </div>
    @error('category_id')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection