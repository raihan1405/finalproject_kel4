<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Forum</title>
</head>
<body>
  @include('sweetalert::alert', ['cdn' => "https://cdn.jsdelivr.net/npm/sweetalert2@9"])
    {{-- Nav1 --}}
    <!-- As a heading -->
    <nav class="navbar navbar-light bg-light justify-content-center" >
        <span class="navbar-brand mb-0 h1">AskUy</span>
    </nav>
    
    {{-- nav2 --}}
    <nav class="navbar navbar-light bg-light justify-content-center">
        <nav class="navbar navbar-light bg-light mr-3">
            <a class="navbar-brand" href="/categorie">
                <img src="{{asset('/image/logo.png')}}" width="60" height="30" alt="">
            </a>
        </nav>
        
        @php
        $user = Auth::user();

        @endphp


        @if (!Auth::check())
        {{-- User is not authenticated --}}
        <div class="nav-item dropdown ml-3">
          <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><span class="navbar-toggler-icon"></a>
        <div class="dropdown-menu">
         
          <a class="dropdown-item" href="/login">Login</a>
          </div>
          </div>
          <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person-circle mr-2" viewBox="0 0 16 16">
            <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z"/>
            <path fill-rule="evenodd" d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z"/>
            </svg>
            guest
         @else
          <div class="nav-item dropdown ml-3">
          <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><span class="navbar-toggler-icon"></a>
          <div class="dropdown-menu">
          <a class="dropdown-item" href="/profile">Profile</a>
            <a class="dropdown-item" href="{{ route('logout') }}"  onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">Logout</a>
          </div>
          <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
              @csrf
          </form>
          </div> 
     
          <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person-circle mr-2" viewBox="0 0 16 16">
          <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z"/>
          <path fill-rule="evenodd" d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z"/>
          </svg>
          {{$user->name}}
      @endif

          
        
    </nav>

   {{-- sidebar --}}
    <main class="container">
        <div class="row">
          <!-- kolom1 -->
          <div class="col-2 ">
            <div class="row my-3 ">
                <div class="card">
                    <div class="card-body">
                      <h5 class="card-title">AsKuy</h5>
                      <hr>
                      <a href="/question" class="btn btn-primary btn-block btn-lg" style="font-size: 16px">PERTANYAAN</a>
                      <a href="/categorie" class="btn btn-primary btn-block btn-lg" style="font-size: 16px">KATEGORI</a>
                    </div>
                  </div>                  
            </div>
           
          </div>
          <div class="card col-9 my-3 ml-3">
            <div class="card-body">
              <blockquote class="blockquote mb-0">
                @yield('categories')
              </blockquote>
            </div>
          </div>
         
    </main>
          
    
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</body>
</html>