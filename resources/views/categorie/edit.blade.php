@extends('layout.master')

@section('categories')
<form action="/categorie/{{$kategori->id}}" method="post">
    @csrf
    @method('put')
    <div class="form-group my-3">
      <label>NAME CATEGORY</label>
      <input type="text" name="name" value="{{$kategori->name}}" class="form-control @error('name') is-invalid   
      @enderror" >
    </div>
    @error('name')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">update categorie</button>
  </form>
@endsection