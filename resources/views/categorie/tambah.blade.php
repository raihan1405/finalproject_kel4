@extends('layout.master')

@push('script')
<script>
  Swal.fire({
    title: "Berhasil!",
    text: "Menambahkan Kategori",
    icon: "success",
    confirmButtonText: "Cool",
  });
</script>
@endpush


@section('categories')
<form action="/categorie" method="post">
    @csrf
    <div class="form-group">
      <label>NAME CATEGORY</label>
      <input type="text" name="name" class="form-control @error('name') is-invalid   
      @enderror" >
    </div>
    @error('name')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection

 