@extends('layout.master')



@section('categories')
@auth
<a href="/categorie/create" class="btn btn-primary my-3">tambah kategori</a>
@endauth
<hr>
        @forelse ($kategori as $key => $item)
            <div class="card-body my-3 border">
                <div class="row">
                    <div class="col-6">
                        <td class="btn">{{$item->name}}</td>
                    </div>
                    <div class="col-6">
                        <form action="/categorie/{{$item->id}}" method="post">
                            @method('delete')
                            @csrf
                            <a href="/categorie/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                            @auth
                            <a href="/categorie/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                            <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                            @endauth
                        </form>
                    </div>
                  </div>
                <tr>
            </div>
        @empty
          <h1>Data Kategori Kosong</h1>
        @endforelse

@endsection