@extends('layout.master')

@section('categories')

<a href="/question/create" class="btn btn-primary my-3">tambah pertanyaan</a>

<div class="row">
    @forelse ($question as $item)
        <div class="col-4 ">
            <div class="card" >
                <img class="card-img-top" src="{{asset('uploads/' . $item->gambar)}}" height="250px">
                <div class="card-body">
                    <h5 class="card-title"><span class="badge badge-primary">{{$kategori->name}}</span></h5>
                  <p class="card-text">{{Str::limit($item->tulisan,50)}}</p>
                  <a href="/question/{{$item->id}}" class="btn btn-info btn-block">Read more</a>
                  @auth
                  <div class="row my-3">
                    <div class="col">
                        <a href="/question/{{$item->id}}/edit" class="btn btn-warning btn-block">Edit</a>
                    </div>
                    <div class="col">
                        <form action="/question/{{$item->id}}" method="POST">
                        @csrf
                        @method('delete')
                        <input type="submit" value="delete" class="btn btn-danger btn-block">
                    </form>
                    </div>

                  </div>
                  @endauth
                </div>
              </div>
        </div>
    @empty
        
    @endforelse
</div>
@endsection