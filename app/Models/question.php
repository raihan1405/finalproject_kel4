<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class question extends Model
{
    use HasFactory;

    protected $table = 'pertanyaan';
    protected $fillable = ['tulisan','gambar','user_id','kategori_id'];


    public function jawaban()
    {
        return $this->hasMany(jawaban::class, 'pertanyaan_id');
    }
    public function kategori(){
        return $this->belongsTo(kategori::class, 'kategori_id');
    }

}