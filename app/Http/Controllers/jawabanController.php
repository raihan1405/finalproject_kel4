<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\File;
use App\Models\jawaban;
use App\Models\question;

class jawabanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index','show']);
    }

    public function tambah(Request $request, $pertanyaan_id)
    {
        $request->validate([
            'content' => 'required|min:5',
            
        ]);
        $idUser = Auth::id();
        $jawaban = new jawaban;

        $jawaban->content = $request->input('content');
        $jawaban->user_id = $idUser;
        $jawaban->pertanyaan_id = $pertanyaan_id;
        $jawaban->save();

        return redirect('/question/'. $pertanyaan_id);
    }
    
    public function destroy($id){
        $jawaban = jawaban::find($id);
        $jawaban->delete();

        $question = question::find($jawaban->pertanyaan_id);

        return redirect('/question/'. $question->id);
        
    }

    public function edit($id){
        $jawaban = DB::table('jawaban')->find($id);
        return view('jawaban.edit',['jawaban'=>$jawaban]);
    }
    
    public function update(Request $request, $id)
    {

        $request->validate([
            'content' => 'required|min:5',
            
        ]);

        $jawaban = jawaban::find($id);
        $idUser = Auth::id();
 
        $jawaban->content = $request->input('content');
        
        $jawaban->save();

        $question = question::find($jawaban->pertanyaan_id);

        return redirect('/question/'. $question->id);
    }

    public function index()
    {

    }

}
