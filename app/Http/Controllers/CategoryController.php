<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use illuminate\Database\Query\Builder;
use App\Models\question;
use RealRashid\SweetAlert\Facades\Alert;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index','show']);
    }

    public function create()
    {
        return view('categorie.tambah');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' =>'required',
        ]);

        $user = Auth::user();
        DB::table('kategori')->insert([
            'name' => $request->input('name'),
            'user_id' =>$user->id,
        ]);

        return redirect('/categorie');
    }

    public function index()
    {
       
        $kategori = DB::table('kategori')->get();
 
        return view('categorie.tampil', ['kategori' => $kategori]);
    }

    public function show($id)
    {
        $question = DB::table('pertanyaan')->where('kategori_id', '=', $id)->get();
        $kategori = DB::table('kategori')->find($id);

        return view('categorie.detail',['question' => $question,'kategori' => $kategori]);
    }

    public function edit($id)
    {
        $kategori = DB::table('kategori')->find($id);

        return view('categorie.edit',['kategori' => $kategori]);
    }


    public function update($id,Request $request)
    {
        $request->validate([
            'name' =>'required',
        ]);

        DB::table('kategori')
              ->where('id', $id)
              ->update(
                [
                    'name' => $request->input('name'),
                ]);
        
        return redirect('/categorie');
    }

    public function destroy($id)
    { 
        Alert::alert('STATUS', 'Kategori Telah Dihapus', 'Type');
        DB::table('kategori')->where('id', '=', $id)->delete();
    
        return redirect('/categorie');
    }
}
