<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\kategori;
use App\Models\question;
use File;

class QuestionController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->except(['index','show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $question = question::all();

        return view('question.tampil',['question'=> $question]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categorie = kategori::all();

        return view('question.tambah',['categorie' => $categorie]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'tulisan' =>'required',
            'category_id' =>'required',
            'gambar' =>'required|mimes:png,jpg,jpeg|max:2048',
        ]);

        $imageName = time(). '.' .$request->gambar->extension();
        $request->gambar->move(public_path('uploads'),$imageName);

        $user = Auth::user();

        $question = new question;
        $question->tulisan = $request->input('tulisan');
        $question->kategori_id = $request->input('category_id');
        $question->user_id = $user->id;
        $question->gambar = $imageName;

        $question->save();

        return redirect('/question');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $question = question::find($id);

        return view('question.detail',['question' => $question]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $question = question::find($id);
        $categorie = kategori::all();
        return view('question.edit',['question' => $question ,'categorie' => $categorie ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'tulisan' =>'required',
            'category_id' =>'required',
            'gambar' =>'mimes:png,jpg,jpeg|max:2048',
        ]);

        $question = question::find($id);
        if ($request->has('gambar')){
            $path = 'uploads/';
            File::delete($path . $question->gambar);

            $imageName = time(). '.' .$request->gambar->extension();
            $request->gambar->move(public_path('uploads'),$imageName);

            $question->gambar = $imageName;
        }

        $user = Auth::user();
        $question->tulisan = $request->input('tulisan');
        $question->kategori_id = $request->input('category_id');
        $question->user_id = $user->id;

        $question->save();

        return redirect('/question');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $question = question::find($id);
        $path = 'uploads/';
        File::delete($path . $question->gambar);
        $question -> delete();

        return redirect('/question');
    }
}
