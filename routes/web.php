<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\QuestionController;
use App\Http\Controllers\jawabanController;
use App\Http\Controllers\HomeController;

use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\LogoutController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/',[HomeController::class,'index']);
//CRUD Category
//CREATE
Route::get('/categorie/create',[CategoryController::class, 'create']);
Route::post('/categorie',[CategoryController::class, 'store']);

//READ
Route::get('categorie',[CategoryController::class, 'index']);
Route::get('categorie/{id}',[CategoryController::class, 'show']);

//UPDATE
Route::get('/categorie/{id}/edit',[CategoryController::class, 'edit']);
Route::put('/categorie/{id}',[CategoryController::class, 'update']);

//DELETE
Route::delete('/categorie/{id}',[CategoryController::class, 'destroy']);
Auth::routes();
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//CRUD Profile
Route::resource('profile', ProfileController::class);

//CRUD ANSWER
Route::resource('question', QuestionController::class);
Auth::routes();


//jawaban
Route::post('/jawaban/{question_id}', [jawabanController::class, 'tambah']);

Route::get('jawaban',[jawabanController::class, 'index']);


Route::get('jawaban/{question_id}/edit',[jawabanController::class, 'edit']);
Route::put('/jawaban/{question_id}',[jawabanController::class, 'update']);

Route::delete('/jawaban/{question_id}',[jawabanController::class, 'destroy']);

Route::get('/home', [CategoryController::class, 'index'])->name('home');

//CRUD logout
Route::post('logout', [LogoutController::class, 'logout'])->name('logout');